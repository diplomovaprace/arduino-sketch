#include <AllThingsTalk_LoRaWAN.h>                      // Load the AllThingsTalk LoRaWAN SDK
#include "keys.h"                                       // Load the header file that contains your credentials for LoRaWAN
#include <Wire.h>                                       // Library used for I2C communication
#include "Adafruit_BME280.h"                            // Adafruit's library for the BME280 Humidity, Temperature and Pressure sensor

#define debugSerial     Serial                          // Define the serial interface that's going to be used for Serial monitor (debugging)
#define loraSerial      Serial1                         // Define the serial interface that'll be used for communication with the LoRa module
#define debugSerialBaud 57600                           // Define the baud rate for the debugging serial port (used for Serial monitor)

float  temperature, humidity, pressure;    // Variables used to store our sensor data
//unsigned int sendEvery = 300;                           // Creates a delay so the data is not constantly sent. 

//ABPCredentials credentials(DEVADDR, APPSKEY, NWKSKEY);  // Define the credential variables loaded from the keys.h file
OTAACredentials credentials(DEVEUI, APPEUI, APPKEY);
LoRaModem modem(loraSerial, debugSerial, credentials);  // Define LoRa modem properties
Adafruit_BME280 tph;                                    // Create an "tph" object which will be our TPH (Temperature, Pressure and Humidity) sensor
BinaryPayload payload2;

void setup() {                                          // This function runs only at boot and only once.
  debugSerial.begin(debugSerialBaud);                   // Initialize the debug serial port (for Serial monitor)
  delay(1000);
  while((!debugSerial) && (millis()) < 10000){}         // Wait for the Serial monitor to be open (so you can see all output) and if it isn't open in 10 seconds, run the sketch
  delay(1000);
  while (!modem.init()) { delay(2000); }                // Initialize the modem. If it fails, retry every 1 second until it succeeds
  
  pinMode(GROVEPWR, OUTPUT);                            // Set the secondary row of grove connectors to OUTPUT (so we can turn it on)
  digitalWrite(GROVEPWR, HIGH);                         // Turn on the power for the secondary row of grove connectors (the switched row on the board) 
  tph.begin();                                          // Begin the i2c connection for the BME280 sensor (the Temperature, Pressure and Humidity Sensor)
}

void readSensors() {                                    // Function that we'll call when we want to read the data from all sensors
  temperature = tph.readTemperature();                  // Read the temperature data from the BME280 (TPH) Sensor and save it into the "temperature" variable
  humidity    = tph.readHumidity();                     // Read the humidity data from the BME280 (TPH) Sensor and save it into the "humidity" variable
  pressure    = tph.readPressure()/100.0;               // Read the pressure data from the BME280 (TPH) Sensor, divide it by 100 and save it into the "pressure" variable
}

void sendSensorValues() {                               // Function used to send the data we collected from all the sensors formatted to Cayenne LPP
  payload2.reset();
  
//temperature
  payload2.add(0x01);                                   //channel
  payload2.add(0x67);                                   //temperature identifier in CayenneLPP
  int temperature1 = round(temperature*10);
  if (temperature1<256){payload2.add(0x00);}
  payload2.add(temperature1);

//pressure 
  payload2.add(0x01);
  payload2.add(0x73);
  int pressure1 = round(pressure*10);
  payload2.add(pressure1);

//humidity  
  payload2.add(0x01);
  payload2.add(0x68);
  int humidity1 = round(humidity*10);
  payload2.add(humidity1/5);

  modem.send(payload2); 
}

void displaySensorValues() {                            // Function we'll call to output all the collected data via debug Serial (to see when you open Serial Monitor)
  debugSerial.println("-----------------------");       // A simple line to distinguish between old and new data easily

  debugSerial.print("Temperature: ");
  debugSerial.print(temperature);
  debugSerial.println(" °C");

  debugSerial.print("Humidity: ");
  debugSerial.print(humidity);
	debugSerial.println(" %");

  debugSerial.print("Pressure: ");
  debugSerial.print(pressure);
	debugSerial.println(" hPa");

  delay(200);                                           // A necessary delay so the serial output isn't cut off while outputting
}

void loop() {                                           // This function runs automatically in a continuous loop as long as the device is powered on
  readSensors();                                        // Calls the function that reads all the sensor data
  displaySensorValues();                                // Calls the function that displays all the sensor data via debug serial (to see in Serial monitor)
  sendSensorValues();                                   // Calls the function that sends all the sensor data to AllThingsTalk Maker via LoRa
  //delay(sendEvery*1000);                                // Creates a delay so the data is not constantly sent. The delay is defined in the "sendEvery" variable at the beginning.
}
